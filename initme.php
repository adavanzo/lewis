<?php
/* =============================================================================
 * Lewis <http://lewis.adavanzo.com>
 * Copyright (c) 2020 Andrea Davanzo
 * License MPL v2.0. See the LICENSE file distributed with this source code.
 * ========================================================================== */

declare(strict_types = 1);

const LEWIS_DIR = __DIR__;
const LEWIS_RELEASE = '1.0.0';
const LEWIS_CODENAME = 'Wollemia';
const LEWIS_DATA_DIR = LEWIS_DIR . '/data';

