<?php
/* =============================================================================
 * Lewis <http://lewis.adavanzo.com>
 * Copyright (c) 2020 Andrea Davanzo
 * License MPL v2.0. See the LICENSE file distributed with this source code.
 * ========================================================================== */

declare(strict_types = 1);

function lewis_tld_exists(string $tld): bool
{
  $tld = str_replace(chr(0), '', $tld); /* filter null byte */
  if ('' === $tld) {
    $result = false;
  } else {
    $result = file_exists(LEWIS_DATA_DIR . '/' . $tld);
  }
  return $result;
}

