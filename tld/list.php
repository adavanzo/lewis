<?php
/* =============================================================================
 * Lewis <http://lewis.adavanzo.com>
 * Copyright (c) 2020 Andrea Davanzo
 * License MPL v2.0. See the LICENSE file distributed with this source code.
 * ========================================================================== */

declare(strict_types = 1);

function lewis_tld_list(): array
{
  return array_diff(scandir(LEWIS_DATA_DIR), ['..', '.']);
}

